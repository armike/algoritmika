<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lessons".
 *
 * @property integer $id
 * @property integer $classgroup
 * @property string $name
 * @property string $classtime
 *
 * @property groups $group
 */
class Lesson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lessons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['groupId'], 'integer'],
            [['lessontime'], 'safe'],
            [['name'], 'string', 'max' => 128],
            [['groupId'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['groupId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'groupId' => 'Group',
            'groupName' => 'Group',
            'name' => 'Name',
            'lessontime' => 'Lesson time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'groupId']);
    }

    public function getGroupName()
    {
        return $this->group->name;
    }

    public function getTeacherName()
    {
        return $this->group->getTeacherName();
    }
}
