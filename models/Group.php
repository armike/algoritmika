<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "classgroups".
 *
 * @property integer $id
 * @property integer $teacher
 * @property string $name
 *
 * @property Teachers $teacher
 * @property Lessons[] $lessons
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacherId'], 'integer'],
            [['name'], 'string', 'max' => 32],
            [['teacherId'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['teacherId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacherId' => 'Teacher',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacherId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lesson::className(), ['groupId' => 'id']);
    }

    public function getTeacherName()
    {
        return $this->teacher->name;
    }

}
