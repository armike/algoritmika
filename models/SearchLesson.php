<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lesson;
use yii\db\Expression;

/**
 * SearchLesson represents the model behind the search form about `app\models\Lesson`.
 */
class SearchLesson extends Lesson
{

    public $groupName;
    public $teacherName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacherName', 'groupName', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lesson::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'lessontime' => [
                        'asc' => ['lessontime' => SORT_ASC],
                        'desc' => ['lessontime' => SORT_DESC],
                        'default' => SORT_ASC,
                        'label' => 'lessontime',
                    ]
                ],
            ],
        ]);
        $dataProvider->query->addOrderBy(new Expression('ISNULL(lessontime) ASC'));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['group']);
            return $dataProvider;
        }

        $query->joinWith(['group' => function ($q) {
            $q->where('groups.name LIKE "%' . $this->groupName . '%"');
        }])->joinWith(['group.teacher' => function ($q) {
            $q->where('teachers.name LIKE "%' . $this->teacherName . '%"');
        }]);

        $query->andFilterWhere(['like', 'lessons.name', $this->name]);

        return $dataProvider;
    }
}
