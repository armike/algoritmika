<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use app\models\Lesson;
use app\models\SearchLesson;

class LessonController extends Controller
{
    public function actionIndex()
    {
        $query = Lesson::find();

        $searchModel = new SearchLesson();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
}